import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Usuario } from "../../models/usuario";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  formularioDeUsuario: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.criarFormularioDeUsuario();
  }

  enviarDados() {
    const dadosFormulario = this.formularioDeUsuario.value;

    const usuario = new Usuario(
      dadosFormulario.nome,
      dadosFormulario.email,
      dadosFormulario.senha
    );

    alert(`O usuário ${usuario.nome} foi cadastrado com sucesso.`);

    this.formularioDeUsuario.reset();

    console.log(this.formularioDeUsuario.value);
  }

  criarFormularioDeUsuario() {
    this.formularioDeUsuario = this.fb.group(
      {
        nome: [
          "",
          Validators.compose([Validators.required, Validators.minLength(6)]),
        ],
        email: ["", Validators.compose([Validators.email])],
        senha: [
          "",
          Validators.compose([Validators.required, Validators.minLength(6)]),
        ],
        confirmarSenha: ["", Validators.compose([Validators.required])],
      },
      {
        validator: this.senhasCombinam,
      }
    );
  }

  senhasCombinam(controle: AbstractControl) {
    let senha = controle.get("senha").value;
    let confirmarSenha = controle.get("confirmarSenha").value;

    if (senha === confirmarSenha) return null;

    controle.get("confirmarSenha").setErrors({ senhasNaoCoincidem: true });
  }

  get nome() {
    return this.formularioDeUsuario.get("nome");
  }

  get email() {
    return this.formularioDeUsuario.get("email");
  }

  get senha() {
    return this.formularioDeUsuario.get("senha");
  }

  get confirmarSenha() {
    return this.formularioDeUsuario.get("confirmarSenha");
  }
}
